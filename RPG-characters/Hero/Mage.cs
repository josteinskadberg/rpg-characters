﻿using RPG_characters.Eqiupment.armor;
using RPG_characters.Eqiupment.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Hero
{
    public class Mage: BaseHero
    {
        public Mage(string name)
        {
            Name = name;
            BaseStats = new PrimaryAtributes
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };

            StatsLevelScaling = new PrimaryAtributes
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 5
            };

            ArmorProficiency = new ArmorTypes.ArmorType[] { ArmorTypes.ArmorType.Cloth };
            WeaponProficiency = new WeaponTypes.WeaponType[] { WeaponTypes.WeaponType.Wand, WeaponTypes.WeaponType.Staff };
            TotalStats = UpdateTotalAttribute();
        }
        public override double getDamage()
        {
            return CalculateDamage(TotalStats.Intelligence);
        }
    }
}
