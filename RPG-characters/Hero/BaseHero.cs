﻿using RPG_characters.CustomExceptions;
using RPG_characters.Eqiupment;
using RPG_characters.Eqiupment.armor;
using RPG_characters.Eqiupment.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace RPG_characters.Hero
{
    public abstract class BaseHero
    {
        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public PrimaryAtributes BaseStats { get; set; }
        public PrimaryAtributes TotalStats { get; set; }
        public PrimaryAtributes StatsLevelScaling { get; set; }
        public WeaponTypes.WeaponType[] WeaponProficiency { get; set; }
        public ArmorTypes.ArmorType[] ArmorProficiency { get; set; }
        
        public Dictionary<Slot, Item> Equipment = new();

        /// <summary>
        /// Increment hero level. 
        /// Base stats is increased based on StatLevelScaling attribute pr lvl
        /// for this hero. 
        /// </summary>
        /// <exception cref="ArgumentException">if given argumen 0 or less</exception>
        /// <param name="lvlIncrease">Number of levels to level up</param>
        public void LevelUp(int lvlIncrease = 1)
        {
            if (lvlIncrease < 1)
            {
                throw new ArgumentException("level increase must be a positive number");
            }
            Level += lvlIncrease;
            for (int i = 0; i < lvlIncrease; i++)
            {
                BaseStats += StatsLevelScaling;
            }
        }

        /// <summary>
        /// Equip a weapon 
        /// </summary>
        /// <param name="weapon">A weapon object</param>
        /// <exception cref="InvalidWeaponException">When Hero level to low or weapon type not in WeaponProficiency</exception>
        /// <returns>A success meassage</returns>
        public string EquipWeapon(Weapon weapon)
        {
            if (weapon.LevelToEquip > Level)
            {
                throw new InvalidWeaponException("You need to level before you can use this weapon");
            }
            if (WeaponProficiency.Contains(weapon.Type))
            {
                Equipment[weapon.Slot] = weapon;
            }
            else
            {
                throw new InvalidWeaponException("Your character class is not proficient with this weapon");
            }

            return "New weapon equipped!";
        }


        /// <summary>
        /// Equip A pice of armor 
        /// </summary>
        /// <param name="armor">an armor object</param>
        /// <exception cref="InvalidArmorException">When Hero level to low or armor type not in ArmorProficiency</exception>
        /// <returns>A success meassage</returns>
        public string EquipArmor(Armor armor)
        {
            if (armor.LevelToEquip > Level)
            {
                throw new InvalidArmorException("You need to level before you can use this weapon");
            }
            if (ArmorProficiency.Contains(armor.Type))
            {
                Equipment[armor.Slot] = armor;
                TotalStats = UpdateTotalAttribute(); 
            }
            else
            {
                throw new InvalidArmorException("Your character class is not proficient with this weapon");
            }
            return "New armor equipped!";
        }

        public abstract double getDamage(); 

        /// <summary>
        /// Calculates the damages output of a hero with or without weapon/armour
        /// </summary>
        /// <param name="primaryAttr">The primary attribute that determin dmg, different based on class</param>
        /// <returns>Damage in double</returns>
        protected double CalculateDamage(double primaryAttr)
        {
            if (Equipment.ContainsKey(Slot.weapon))
            {
                Weapon currentWeapon = (Weapon)Equipment[Slot.weapon];
                return currentWeapon.DPS * (1 + (primaryAttr / 100));
            }

            return 1*(1 + (primaryAttr / 100)); 

           
        }

        /// <summary>
        /// Calculates the total attribute (baseStats + stats from armor)
        /// </summary>
        /// <returns>Aggregated attributes</returns>
        public PrimaryAtributes UpdateTotalAttribute()
        {
            PrimaryAtributes totalAttribute = BaseStats;
            foreach (KeyValuePair<Slot, Item> entry in Equipment)
            {
                if (entry.Value.GetType() == typeof(Eqiupment.armor.Armor)){
                    Armor armorPiece = (Armor)entry.Value;
                    totalAttribute += armorPiece.Attributes;
                }
            }
            return totalAttribute;

        }

        

    }
}
