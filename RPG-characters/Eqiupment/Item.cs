﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Eqiupment
    
{   
    /// <summary>
    /// Contains attributes shared by all items (weapons and armor)
    /// </summary>
   public abstract class Item
    {
        public String Name { get; set; }
        public int LevelToEquip { get; set; }
        public Slot Slot { get; set; }
    }

    public enum Slot
    {
        head,
        body,
        legs, 
        weapon, 
    }

}