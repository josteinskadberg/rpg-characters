﻿using RPG_characters.Eqiupment;
using RPG_characters.Eqiupment.armor;
using RPG_characters.Eqiupment.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Hero
{
    public class Warrior:BaseHero
    {
        public Warrior(string name)
        {
            Name = name;
            BaseStats = new PrimaryAtributes
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };
           
            StatsLevelScaling = new PrimaryAtributes
            {
                Strength = 3,
                Dexterity = 2,
                Intelligence = 1
            };

            ArmorProficiency = new ArmorTypes.ArmorType[] { ArmorTypes.ArmorType.Mail, ArmorTypes.ArmorType.Plate };
            WeaponProficiency = new WeaponTypes.WeaponType[] { WeaponTypes.WeaponType.Axe, WeaponTypes.WeaponType.Sword, WeaponTypes.WeaponType.Hammer };
            TotalStats = UpdateTotalAttribute();
        }

        public override double getDamage()
        {
            return CalculateDamage(TotalStats.Strength); 
        }
    }
}
