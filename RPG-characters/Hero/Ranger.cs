﻿using RPG_characters.Eqiupment.armor;
using RPG_characters.Eqiupment.weapon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Hero
{
   public class Ranger: BaseHero
    {
        public Ranger(string name)
        {
            Name = name;
            BaseStats = new PrimaryAtributes
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1
            };

            StatsLevelScaling = new PrimaryAtributes
            {
                Strength = 1,
                Dexterity = 5,
                Intelligence = 1
            };

            ArmorProficiency = new ArmorTypes.ArmorType[] { ArmorTypes.ArmorType.Mail, ArmorTypes.ArmorType.Leather };
            WeaponProficiency = new WeaponTypes.WeaponType[] { WeaponTypes.WeaponType.Bow };
            TotalStats = UpdateTotalAttribute();
        }
        public override double getDamage()
        {
            return CalculateDamage(TotalStats.Dexterity);
        }
    }
}
