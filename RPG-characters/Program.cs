﻿using RPG_characters.Eqiupment;
using RPG_characters.Eqiupment.armor;
using RPG_characters.Eqiupment.weapon;
using RPG_characters.Hero;
using System;
using System.Text;

namespace RPG_characters
{
    class Program
    {
        static void Main(string[] args)
        {

            BaseHero hero = new Warrior("The First Hero");
            PrimaryAtributes startItem = new()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 1
            };
            Armor plate_body = new(ArmorTypes.ArmorType.Plate, startItem, Slot.body, 1, "Basic Plate body");
            Armor plate_boots = new(ArmorTypes.ArmorType.Plate, startItem, Slot.legs, 1, "Basic Plate boots");
            Armor plate_helmet = new(ArmorTypes.ArmorType.Plate, startItem, Slot.head, 1, "Basic Plate helmet");
            Weapon sword = new("basic Sword", 1, 7, 1.1, WeaponTypes.WeaponType.Sword);
            hero.EquipWeapon(sword);
            hero.EquipArmor(plate_body);
            hero.EquipArmor(plate_boots);
            hero.EquipArmor(plate_helmet);

            Console.WriteLine(DisplayStatSheet(hero));
        }

        /// <summary>
        /// Constructs a hero spesific stat sheet. 
        /// </summary>
        /// <param name="hero">a hero object</param>
        /// <returns> String builder with stat sheet</returns>
        public static StringBuilder DisplayStatSheet(BaseHero hero)
        {
            StringBuilder statSheet = new StringBuilder();
            statSheet.AppendLine("------------Stats------------");
            statSheet.AppendLine($"Name : {hero.Name}");
            statSheet.AppendLine($"Level: {hero.Level}");
            statSheet.AppendLine($"Damage: {Math.Round(hero.getDamage(),1)}");
            statSheet.AppendLine($"\n\tBase Stats");
            statSheet.AppendLine($"  Strength:\t{hero.TotalStats.Strength.ToString()}");
            statSheet.AppendLine($"  Dexterity:\t{hero.TotalStats.Dexterity.ToString()}");
            statSheet.AppendLine($"  Intelligence:\t{hero.TotalStats.Intelligence.ToString()}");
            statSheet.AppendLine("-----------------------------");
            return statSheet;
        }
        
    }
       
}
