﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Eqiupment.weapon
{
    /// <summary>
    /// Weapon that can be equiped in the weapon slot. Increase damage output
    /// </summary>
    public class Weapon:Item
    {
        public double BaseDamage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get; set; }
        public WeaponTypes.WeaponType Type { get; set; }

        public Weapon(string name, int lvlToEquip, double baseDamage, double attackSpeed, WeaponTypes.WeaponType type)
        {
            BaseDamage = baseDamage;
            AttackSpeed = attackSpeed;
            DPS = BaseDamage*AttackSpeed;
            Name = name;
            LevelToEquip = lvlToEquip;
            Slot = Slot.weapon;
            Type = type;


        }

        public Weapon()
        {
        }
    }

   
}
