﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters
{
    /// <summary>
    /// A class that represents the three base stats for each hero. They are used for characters and armor. 
    /// </summary>
    public class PrimaryAtributes
    {
        public double Strength { get; set; }
        public double Dexterity { get; set; }
        public double Intelligence { get; set; }

     
        /// <param name="obj"></param>
        /// <returns>True if all attribute values are equal for two primaryAttribute objects</returns>
        public override bool Equals(object obj)
        {
            return obj is PrimaryAtributes atributes &&
                   Strength == atributes.Strength &&
                   Dexterity == atributes.Dexterity &&
                   Intelligence == atributes.Intelligence;
        }

        /// <summary>
        /// Override + operator to aggregate the attribute values of two Primaryattribute objects. 
        /// Each value are added together with coresponding value in other object.
        /// </summary>
        /// <param name="lhs"></param>
        /// <param name="rhs"></param>
        /// <returns>A object containing the aggregated values</returns>
        public static PrimaryAtributes operator +(PrimaryAtributes lhs, PrimaryAtributes rhs)
        {
            return new PrimaryAtributes
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence
            };
        }



    }
}
