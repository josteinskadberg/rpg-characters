using RPG_characters;
using RPG_characters.Hero;
using System;
using Xunit;
namespace TestRPG
{
    public class CharacterTest
    {
        [Fact]
        public void HeroConstructor_IntiatingNewRanger_ShouldCreateRangerLevelOne()
        {
            int expectedLevel = 1;
            Ranger ranger = new("TestLevel1");

            int actualLevel = ranger.Level;

            Assert.Equal<int>(actualLevel, expectedLevel); 
        }



        [Fact]
        public void LevelUp_HeroSingleLevelUp_ShouldIncrementLevelBy1()
        {
            int expectedLevel = 2;
            BaseHero warior = new Warrior("TestlevelIncrement");

            warior.LevelUp();
            int actualLevel = warior.Level;

            Assert.Equal<int>(actualLevel, expectedLevel);
        }
        
        [Theory]
        [InlineData(2)]
        [InlineData(5)]
        public void LevelUp_HeroMultibleLevelUp_ShouldIncrementLevelBySize(int lvlUpTimes)
        {
            int expectedLevel = 1 + lvlUpTimes;
            BaseHero warior = new Warrior("TestlevelIncrement");

            warior.LevelUp(lvlUpTimes);
            int actualLevel = warior.Level;

            Assert.Equal<int>(actualLevel, expectedLevel);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_IncorrectInput_ShouldThrowArgumentException(int lvlUpTimes)
        {
            Warrior warrior = new("IncorrectLevelTest");
            Assert.Throws<ArgumentException>(() => warrior.LevelUp(lvlUpTimes)); 
        }

        [Fact]
        public void ConstuctorRanger_BaseStatsOnInit_ShouldReturnCorrectBaseStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 1,
                Dexterity = 7,
                Intelligence = 1
            };
            Ranger ranger = new("TestBaseStats");

            PrimaryAtributes actualStats = ranger.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }

        [Fact]
        public void ConstuctorWarrior_BaseStatsOnInit_ShouldReturnCorrectBaseStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 5,
                Dexterity = 2,
                Intelligence = 1
            };
            Warrior warrior = new("TestBaseStats");

            PrimaryAtributes actualStats = warrior.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }

        [Fact]
        public void ConstuctorMage_BaseStatsOnInit_ShouldReturnCorrectBaseStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 1,
                Dexterity = 1,
                Intelligence = 8
            };
            Mage mage = new("TestBaseStats");

            PrimaryAtributes actualStats = mage.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }

        [Fact]
        public void ConstuctorRogue_BaseStatsOnInit_ShouldReturnCorrectBaseStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 2,
                Dexterity = 6,
                Intelligence = 1
            };
            Rogue rogue = new("TestBaseStats");

            PrimaryAtributes actualStats = rogue.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }


        [Fact]
        public void LevelUp_RangerIncreaseStats_ShouldReturnCorrectlyIncreasedStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 2,
                Dexterity = 12,
                Intelligence = 2
            };
            Ranger ranger = new("TestBaseStats");
            ranger.LevelUp(); 
            PrimaryAtributes actualStats = ranger.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }

        [Fact]
        public void LevelUp_WarrriorIncreaseStats_ShouldReturnCorrectlyIncreasedStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 8,
                Dexterity = 4,
                Intelligence = 2
            };
            Warrior warrior = new("TestBaseStats");
            warrior.LevelUp();
            PrimaryAtributes actualStats = warrior.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }

        [Fact]
        public void LevelUp_MageIncreaseStats_ShouldReturnCorrectlyIncreasedStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 2,
                Dexterity = 2,
                Intelligence = 13 
            };
            Mage mage = new("TestBaseStats");
            mage.LevelUp();
            PrimaryAtributes actualStats = mage.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }

        [Fact]
        public void LevelUp_RogueIncreaseStats_ShouldReturnCorrectlyIncreasedStats()
        {
            PrimaryAtributes expectedStats = new()
            {
                Strength = 3,
                Dexterity = 10,
                Intelligence = 2
            };
            Rogue rogue = new("TestBaseStats");
            rogue.LevelUp();
            PrimaryAtributes actualStats = rogue.BaseStats;

            Assert.True(expectedStats.Equals(actualStats));
        }
    }

}