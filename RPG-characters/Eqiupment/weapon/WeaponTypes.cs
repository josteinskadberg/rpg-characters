﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Eqiupment.weapon
{
    /// <summary>
    /// Currently available types of weapons
    /// </summary>
    public class WeaponTypes
    {
        public enum WeaponType{
            Axe,
            Bow,
            Dagger,
            Staff,
            Sword,
            Wand,
            Hammer
        }
    }
}

