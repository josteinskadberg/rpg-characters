﻿using RPG_characters;
using RPG_characters.CustomExceptions;
using RPG_characters.Eqiupment;
using RPG_characters.Eqiupment.armor;
using RPG_characters.Eqiupment.weapon;
using RPG_characters.Hero;
using System;
using Xunit;

namespace TestRPG
{

    public class EquipmentTests
    {
        [Fact]
        public void EquipWeapon_WeaponLevelRequirmentLargerThanHeroLevel_ThrowInvalidWeaponException()
        {
            Warrior warrior = new("Overeager Warroir");
            Weapon testAxe = new()
            {
                Name = "The Rejecter",
                LevelToEquip = 2,
                AttackSpeed = 1.1,
                BaseDamage = 7,
                Slot = Slot.weapon,
                Type = WeaponTypes.WeaponType.Axe,
            };

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testAxe));
        }

        [Fact]
        public void EquipArmor_ArmorLevelRequirmentLargerThanHeroLevel_ThrowInvalidWeaponException()
        {
            Warrior warrior = new("Overeager Warroir");
            Armor testPlate = new()
            {
                Attributes = new PrimaryAtributes() { Strength = 1, Dexterity = 1, Intelligence = 1 },
                LevelToEquip = 2,
                Name = "Stuborn Armor",
                Slot = Slot.body,
                Type = ArmorTypes.ArmorType.Plate,
            };
            
            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testPlate));
        }

        [Fact]
        public void EquipWeapon_InvalidWeaponType_ThrowInvalidWeaponException()
        {
            Warrior warrior = new("Overeager Warrior");
            Weapon testBow = new()
            {
                Name = "Common Bow",
                LevelToEquip = 1,
                AttackSpeed = 0.8,
                BaseDamage = 12,
                Slot = Slot.weapon,
                Type = WeaponTypes.WeaponType.Bow,
            };

            Assert.Throws<InvalidWeaponException>(() => warrior.EquipWeapon(testBow));
        }

        [Fact]
        public void EquipArmor_ArmorTypeInvalid_ThrowInvalidWeaponException()
        {
            Warrior warrior = new("Poor Warrior");
            Armor testCloth = new()
            {
                Attributes = new PrimaryAtributes() { Strength = 1, Dexterity = 1, Intelligence = 1 },
                LevelToEquip = 1,
                Name = "Clothbag",
                Slot = Slot.body,
                Type = ArmorTypes.ArmorType.Cloth,
            };

            Assert.Throws<InvalidArmorException>(() => warrior.EquipArmor(testCloth));

            
        }

        [Fact]
        public void EquipWeapon_ValidWeaponEquip_ReturnCorrectStringValue()
        {
            string exptected = "New weapon equipped!";
           
            Warrior warrior = new("Newbie");
            Weapon testSword = new()
            {
                Name = "Fast azz sword",
                LevelToEquip = 1,
                AttackSpeed = 1.8,
                BaseDamage = 8,
                Slot = Slot.weapon,
                Type = WeaponTypes.WeaponType.Sword,
            };

            string acctual = warrior.EquipWeapon(testSword);

            Assert.Equal(exptected, acctual);
        }

        [Fact]
        public void EquipArmor_ValidArmorEquip_ReturnCorrectStringValue()
        {
            string exptected = "New armor equipped!";

            Warrior warrior = new("Newbie");
            Armor testPlate = new()
            {
                Attributes = new PrimaryAtributes() { Strength = 1, Dexterity = 1, Intelligence = 1 },
                LevelToEquip = 1,
                Name = "Platy",
                Slot = Slot.body,
                Type = ArmorTypes.ArmorType.Plate,
            };

            string acctual = warrior.EquipArmor(testPlate);

            Assert.Equal(exptected, acctual);
        }

        [Fact]
        public void GetDamage_DamageWithNoEquipedWeapon_ReturnCorrectNumber()
        {
            double expected = 1 * (1 + (5.0 / 100.0));

            Warrior warrior = new("Newbie");
            double actual = warrior.getDamage();

            Assert.Equal(expected, actual); 
        }

        [Fact]
        public void GetDamage_DamageCalcWithWeapon_ReturnCorrectNumber()
        {
            double expected =  (7.0 * 1.1) * (1.0 + (5.0 / 100.0));

            Warrior warrior = new("Newbie");
         
            Weapon testAxe= new("Big Damage", 1, 7, 1.1, WeaponTypes.WeaponType.Axe);
            warrior.EquipWeapon(testAxe);
            double acctual = warrior.getDamage();

            Assert.Equal(expected, acctual);
        }

        [Fact]
        public void GetDamage_DamageCalcWithWeaponAndArmor_ReturnCorrectNumber()
        {
            double expected = (7.0 * 1.1) * (1.0 + (6.0 / 100.0));

            Warrior warrior = new("Newbie");

            Armor testPlate = new()
            {
                Attributes = new PrimaryAtributes() { Strength = 1, Dexterity = 1, Intelligence = 1 },
                LevelToEquip = 1,
                Name = "Platy",
                Slot = Slot.body,
                Type = ArmorTypes.ArmorType.Plate,
            };
            Weapon testAxe = new("Big Damage", 1, 7, 1.1, WeaponTypes.WeaponType.Axe);
            warrior.EquipWeapon(testAxe);
            warrior.EquipArmor(testPlate);
            double acctual = warrior.getDamage();

            Assert.Equal(expected, acctual);
        }


    }
}
