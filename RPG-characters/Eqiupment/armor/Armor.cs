﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPG_characters.Eqiupment.armor
{
    public class Armor:Item
    {
        /// <summary>
        /// Armor that can be equiped in head, body or legs slot. Increase Hero totalStats.
        /// </summary>
        public Armor()
        {
        }

        public Armor(ArmorTypes.ArmorType type, PrimaryAtributes attributes, Slot slot, int lvlToEquip, string name)
        { 
            Slot = slot;
            LevelToEquip = lvlToEquip;
            Name = name;
            Type = type;
            Attributes = attributes;
        }

        public ArmorTypes.ArmorType Type{ get; set; }
        public PrimaryAtributes Attributes { get; set; }
    }
}
